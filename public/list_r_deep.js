var data = [{
    "title": "MNIST",
    "language": "R + Py",
    "desc": "Chapter 1: MNIST (Modified National Institute of Standards and Technology) is the database of handwritten digits. It is one of the \"hello world\" problems in machine learning. Logistic regression, Keras fully connected network, Keras CNN and activation visualisations.",
    "rmd_notebook_src": "./data/r_deep/mnist.html",
    "py_notebook_src": "./data/r_deep/MNIST_Python.html",
    "value": 2
},{
    "title": "GTSRB",
    "language": "R + Py",
    "desc": "Chapter 2: GTSRB (German Traffic Sign Recognition Benchmark) is a database of pictures of traffic signs. The objective is to create a network that will be able to successfully clasify signs. Keras CNN, dropout, learning decay, data augmentation, activation visualisations.",
    "rmd_notebook_src": "./data/r_deep/GTSRB.html",
    "py_notebook_src": "./data/r_deep/GTSRB_py.html",
    "value": 2
},{
    "title": "Autoencoders",
    "language": "R",
    "desc": "Chapter 3: Autoencoders are very powerful because they learn how to recreate the data. They do it by compressing input data using dimension reduction and then turning it back to its original size. When the reconstruction error is large, we can assume that the occurrence is strange (outlier, fraud). I have used Keras and H2O to create autoencoders.",
    "rmd_notebook_src": "./data/r_deep/fraud_detection.html",
    "py_notebook_src": "",
    "value": 2
}

]

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

$(document).ready(function () {
    createCards();
});

function createCards() {
    console.log("List generator ready");

    var listItemTemplate = $("#list-item-template").html();

    var parentLists = [$("#project-list"), $("#all-project-list")];
    // var parentDeck = $("#card-deck")
    data.sort(dynamicSort("-value"));
    
    $.each(parentLists, function (l, list) {
        if (parentLists[l].length != 0) {
            $.each(data, function (i, item) {
                parentLists[l].append(Mustache.render(listItemTemplate, item));                
            });
        }
    });
}