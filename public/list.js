// AJAX request does not work on GitLab pages so this is a workaround

var data = [
    {
        "id": "0005",
        "title": "House Prices: Advanced Regression Techniques",
        "link": "./data/housePrices.html",
        "tags": [
            {
                "name": "R",
                "link": "./data/housePrices.html"
            }
        ],
        "desc": "This is one of my first bigger projects. The dataset was downloaded from Kaggle. The main point was to learn how to properly fix missing data using different techniques. I have also learned a lot about visualisation and its impacts on understanding the data. I have used various machine learning algorithms in order to predict the data.",
        "value": 2
    },
    {
        "id": "0006",
        "title": "Modest Mouse NLP",
        "link": "https://gitlab.com/tomasz.cichy98/modest-mouse-nlp",
        "tags": [
            {
                "name": "Python",
                "link": "https://gitlab.com/tomasz.cichy98/modest-mouse-nlp"
            },
            {
                "name": "R (old)",
                "link": "./data/R_lyrics_NLP.html"
            },
            {
                "name": "Old version",
                "link": "https://gitlab.com/tomasz.cichy98/the-black-keys-nlp"
            }
        ],
        "desc": "Web scraping using BeautifulSoup4 to get the lyrics. Data cleaning and EDA done with Pandas, matplotlib, and spacy. Topic modeling using LDA from genism. Text generation using Markov Chains and GPT2.",
        "value": 5
    },
    {
        "id": "0004",
        "title": "\"R Deep Learning Projects\"",
        "link": "./r-deep-learning.html",
        "tags": [
            {
                "name": "R",
                "link": "./r-deep-learning.html"
            },
            {
                "name": "GitLab",
                "link": "https://gitlab.com/tomasz.cichy98/r-deep-learning-projects"
            }
        ],
        "desc": "Examples from \"R Deep learning Projects\" by Yuxi Liu and Pablo Maldonado. The book provides code snippets with in-depth explanations of the mathematics behind them. I am recreating these examples in R and Python. They are not an exact copy. Sometimes I explore my own ideas and solutions and sometimes technology does not allow me to perfectly recreate an example. Python notebooks are designed to imitate R notebooks with a fair degree of freedom.",
        "rmd_notebook_src": "./r-deep-learning.html",
        "py_notebook_src": "",
        "value": 5
    },
    {
        "id": "0002",
        "title": "Histopathologic Cancer Detection",
        "link": "./data/Histopathologic_Cancer_Detection.nb.html",
        "tags": [
            {
                "name": "R",
                "link": "./data/Histopathologic_Cancer_Detection.nb.html"
            },
            {
                "name": "Py",
                "link": "./data/Histopathologic_Cancer_Detection_python_fastai.html"
            },
            {
                "name": "GitLab",
                "link": "https://gitlab.com/tomasz.cichy98/histopathologic-cancer-detection"
            }
        ],
        "desc": "Keras Xception model with R and FastAI with Python to classify cancer cells.",
        "value": 6
    },
    {
        "id": "0001",
        "title": "Artificial Intelligence: Reinforcement Learning in Python [Course]",
        "link": "https://gitlab.com/tomasz.cichy98/ai-rl-lazy-programmer",
        "tags": [
            {
                "name": "Udemy",
                "link": "https://www.udemy.com/course/artificial-intelligence-reinforcement-learning-in-python/"
            },
            {
                "name": "GitLab",
                "link": "https://gitlab.com/tomasz.cichy98/ai-rl-lazy-programmer"
            },
            {
                "name": "Certificate",
                "link": "https://www.udemy.com/certificate/UC-e77aeb78-1927-42f4-9c84-e2ea89a9c373/"
            }
        ],
        "desc": "Complete guide to Artificial Intelligence, prep for Deep Reinforcement Learning with Stock Trading Applications.",
        "value": 8
    },
    {
        "id": "0009",
        "title": "Advanced AI: Deep Reinforcement Learning in Python [Course]",
        "link": "https://gitlab.com/tomasz.cichy98/ai-rl-lazy-programmer",
        "tags": [
            {
                "name": "Udemy",
                "link": "https://www.udemy.com/course/deep-reinforcement-learning-in-python"
            },
            {
                "name": "GitLab",
                "link": "https://gitlab.com/tomasz.cichy98/ai-rl-lazy-programmer"
            },
            {
                "name": "Certificate",
                "link": "https://www.udemy.com/certificate/UC-44946680-ea67-40a0-92e5-39f7da71eb7d/"
            }
        ],
        "desc": "The Complete Guide to Mastering Artificial Intelligence using Deep Learning and Neural Networks.",
        "value": 9
    },
    {
        "id": "0010",
        "title": "League Of Legends Draft Assistant (Halted)",
        "link": "https://tomaszcichy.com/draft/",
        "tags": [
            {
                "name": "Python",
                "link": "https://gitlab.com/tomasz.cichy98/league-of-legends-game-result-ml"
            },
            {
                "name": "JS",
                "link": "https://gitlab.com/tomasz.cichy98/league-of-legends-game-result-ml"
            },
        ],
        "desc": "Parallel game data generation using Riot's official API. Game data analysis and export. Champion icon dataset creation and augmentation. Champion icon classification model training end export to ONNX. All of the above done in Python. Progressive Web App creation and serving of the machine learning model (JS).",
        "value": 11
    },
    {
        "id": "0011",
        "title": "Theory and Implementation of Multi-Agent Reinforcement Learning Systems (Final BSc Project)",
        "link": "https://gitlab.aicrowd.com/BernRiemann/flatland-rl",
        "tags": [
            {
                "name": "Python",
                "link": "https://gitlab.aicrowd.com/BernRiemann/flatland-rl"
            },
            {
                "name": "Runs",
                "link": "https://wandb.ai/cichyt/flatland-rl-experiments?workspace=user-cichyt"
            },
            {
                "name": "Report",
                "link": "https://gitlab.aicrowd.com/BernRiemann/flatland-rl/-/blob/afe32f6b2042bc538239542153b4ef65d44eb19b/report/latex/TomaszCichyReport.pdf"
            }
        ],
        "desc": "As a final university project, I have used multi agent reinforcement learning for train control. The problem is hosted by AIcrowd, and is an abstraction of a real world problem that needs to be solved.",
        "value": 12
    }
]

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

$(document).ready(function () {
    createCards();
});

function createCards() {
    console.log("List generator ready");

    var listItemTemplate = $("#list-item-template").html();

    var parentLists = [$("#project-list"), $("#all-project-list")];
    // var parentDeck = $("#card-deck")
    data.sort(dynamicSort("-value"));
    console.log(data);

    // console.log(parentLists);

    // console.log(parentLists[0].length);

    // parentLists is a list of parent lists. If list exists its length is != 0
    // so we are only filling the list that exists

    $.each(parentLists, function (l, list) {
        if (parentLists[l].length != 0) {
            $.each(data, function (i, item) {
                // we don't want items with value less then 1
                // and we do not want more than 3 items on the main site
                // single & for reasons
                if (l == 0 & (item.value < 1 || i > 3)) {
                    return;
                }

                // populate big list
                parentLists[l].append(Mustache.render(listItemTemplate, item));

                // tag list and parent
                tagListTemplate = $("#tag_list_template").html();
                tagListParent = $("#" + item.id.toString());

                // populate tag list
                tagData = item.tags
                $.each(tagData, function (t, tag) {
                    tagListParent.append(Mustache.render(tagListTemplate, tag));
                });
            });
        }
    });
}
